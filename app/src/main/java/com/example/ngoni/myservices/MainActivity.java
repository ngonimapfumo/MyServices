package com.example.ngoni.myservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Button mStart, mStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStart=(Button)findViewById(R.id.start_button);
        mStop=(Button)findViewById(R.id.stop_button);
        mStart.setOnClickListener(this);
        mStop.setOnClickListener(this);
        Log.d(TAG, " <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" +
                "oncreate event");
    }

    public void startService(View v) {
        startService(new Intent(getBaseContext(), TheService.class));
    }

    public void stopService(View v) {
        stopService(new Intent(getBaseContext(), TheService.class));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.start_button:
                startService(v);
                break;

            case R.id.stop_button:
                stopService(v);
                break;
        }
    }
}
